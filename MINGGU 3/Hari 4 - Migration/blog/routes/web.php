<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');



// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/master', function () {
//     return view('adminlte.master');
// });

// Route::get('/items', function () {
//     return view('items.index');
// });

// Route::get('/tables', function () {
//     return view('items.table');
// });

// Route::get('/data-tables', function () {
//     return view('items.data_tables');
// });
