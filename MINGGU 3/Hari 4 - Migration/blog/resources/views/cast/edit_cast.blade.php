@extends('adminlte.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="/cast/{{$casts->id}}" >
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" id="nama_cast" name="nama_cast" placeholder="Masukan Nama Cast" value="{{old('nama_cast', $casts->nama)}}">
                <!-- @error('nama_cast')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror -->
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Umur</label>
                <input type="text" class="form-control " id="umur_cast" name="umur_cast" placeholder="Masukan Umur Cast" value="{{old('umur_cast', $casts->umur)}}">
                <!-- @error('umur_cast')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror -->
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea class="form-control" name="bio_cast" placeholder="Enter ..." value="{{old('bio_cast', $casts->bio)}}"></textarea>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
          	</div>
        </div>
    </form>
</div>
@endsection