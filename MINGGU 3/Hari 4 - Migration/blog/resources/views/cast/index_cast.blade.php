@extends('adminlte.master')

@section('content')
  <div class="card-header">
    <h3 class="card-title">Cast Table</h3>
  </div>
  <div class="card-body">
  <a class="btn btn-primary" href="/cast/create">Create + </a>
  <table class="table table-bordered mt-1">
  <thead>                  
    <tr>
      <th style="width: 10px">id</th>
      <th>Nama Cast</th>
      <th>Umur Cast</th>
      <th>Bio</th>
      <th style="width: 40px">Action</th>
    </tr>
  </thead>
  <tbody>
     @forelse($casts as $key => $casts)
      <tr>
        <td>{{$key + 1}}</td>
        <td>{{$casts->nama}}</td>
        <td>{{$casts->umur}}</td>
        <td>{{$casts->bio}}</td>
        <td><a href="/cast/{{$casts->id}}" class="btn btn-info btn-sm">detail</a>
          <a href="/cast/{{$casts->id}}/edit" class="btn btn-info btn-sm mt-1">edit</a>

          <form action="/cast/{{$casts->id}}" method="post" class="mt-1">
            @csrf
            @method('DELETE')
            <input type="submit" value="Delete" class="btn btn-sm btn-danger">
            
          </form>
        </td>
      </tr>
      @empty
      <p> Data empty</p>
     @endforelse
  </tbody>
  </table>
  </div>
	
@endsection