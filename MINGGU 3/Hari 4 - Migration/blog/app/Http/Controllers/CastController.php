<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
   public function create(){
   	return view('cast.create_cast');
   }
   public function store(Request $request){
   	// dd($request->all());
   	// $request->validate([
   	// 	'nama_cast' => 'required|unique:posts',
    // 	'umur_cast' => 'required'
   	// ]);
   	$query = DB::table('cast')->insert([
    'nama' => $request["nama_cast"],
    'umur' => $request["umur_cast"],
    'bio' => $request["bio_cast"]
	]);

	return redirect('/cast');
   }
   public function show($id){
   	$casts = DB::table('cast')->where('id', $id)->first();
    return view('cast.show_cast', compact('casts'));
   }
   public function edit($id){
   	$casts = DB::table('cast')->where('id', $id)->first();
    return view('cast.edit_cast', compact('casts'));
   }
   public function update($id, Request $request){
   	$query = DB::table('cast')
    ->where('id', $id)
    ->update([
      'nama' => $request["nama_cast"],
      'umur' => $request["umur_cast"],
      'bio' => $request["bio_cast"]
    ]);
    return redirect('/cast');
   }
   public function destroy($id){
   	$casts = DB::table('cast')->get();
    $query = DB::table('cast')
    ->where('id', $id)
    ->delete();
    return redirect('/cast');
   }
   public function index(){
   	$casts = DB::table('cast')->get();
   	// dd($casts);
   	return view('cast.index_cast', compact('casts'));
   }
}
