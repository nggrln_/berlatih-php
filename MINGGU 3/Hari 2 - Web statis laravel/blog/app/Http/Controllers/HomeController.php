<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
   public function home(Request $request){
   		return view('home');
   }

   public function home_post(Request $request){
   		dd($request->all());
   		$namadepan = $request["first_name"];
   		$namabelakang = $request["last_name"];
   		return view('home', ["namadepan" => $namadepan, "namabelakang" => $namabelakang]);
   }


}
