<?php
	require('animal.php');
	require('Ape.php');
	require('Frog.php');

	$sheep = new Animal("shaun");

	echo "Animals Name: ".$sheep->name."<br>"; // "shaun"
	echo "Legs: ".$sheep->legs."<br>"; // 4
	echo "Cold Blooded type: ".$sheep->cold_blooded."<br><br>"; // "no"

	$sungokong = new Ape("kera sakti");
	echo "Animals Name: ".$sungokong->name."<br>";
	echo "Legs :".$sungokong->legs."<br>"; 
	echo "Cold Blooded type: ".$sungokong->cold_blooded."<br>";
	$sungokong->yell(); // "Auooo"
	echo "<br><br>";
	 
	$kodok = new Frog("buduk");
	echo "Animals Name: ".$kodok->name."<br>";
	echo "Legs".$kodok->legs."<br>"; 
	echo "Cold Blooded type".$kodok->cold_blooded."<br>";
	$kodok->jump() ; // "hop hop"

?>